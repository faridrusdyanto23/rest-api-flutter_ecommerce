<?php
include '../config/functions.php';
$namaTabel = "flutter_shopping_cart";
$TabelProduk = "flutter_barang";
$TabelJual = "flutter_penjualan";
$TabelJualDetail = "flutter_penjualan_detail";
header('Content-Type: text/xml');

$UserID = $_POST['userid'];
$grandtotal = $_POST['grandtotal'];
$nilaibayar = $_POST['nilaibayar'];
$nilaikembali = $_POST['nilaikembali'];

if ($UserID != "") {
  // Insert Data Penjualan
  $hasil = $db->query("INSERT INTO $TabelJual VALUES(NULL,'', '$UserID', NOW(), '$grandtotal', '$nilaibayar', '$nilaikembali') ");
  if ($hasil) {
    $id_faktur = "0";
    // Ambil no faktur terakhir
    $sqlF = "SELECT IFNULL(id_faktur,0) id_faktur FROM flutter_penjualan ORDER BY id_faktur DESC limit 1 ";
    $rssqlF = mysqli_query($con, $sqlF);
    while ($f = mysqli_fetch_array($rssqlF)) {
      $id_faktur = $f['id_faktur'];
    }

    // Insert Detail Penjualan
    $rssql = "SELECT a.id_barang id_barang, a.qty qty, a.harga harga, b.nama_barang nama_barang FROM 
      $namaTabel a join $TabelProduk b  on a.id_barang = b.id_barang WHERE a.userid = '$UserID' ";
    $sql = mysqli_query($con, $rssql);
    while($a = mysqli_fetch_array($sql)) {
      $id_barang = $a['id_barang'];
      $qty       = $a['qty'];
      $harga     = $a['harga'];

      $hasildetail = $db->query("INSERT INTO $TabelJualDetail VALUES(NULL, '$id_faktur', '$id_barang', '$qty', '$harga') ");
    }
    if($hasildetail) {
      $ddel = $db->query("DELETE FROM $namaTabel WHERE userid = '$UserID'");

      if($ddel) {
        $response['success'] = 1;
        $response['message'] = "Berhasil Tambah Data";
        echo json_encode($response);
      } else {
        $response['success'] = 0;
        $response['message'] = "Data Cart Gagal Dihapus";
        echo json_encode($response);
      }
    } else {
      $response['success'] = 0;
      $response['message'] = "Data Detail Gagal disimpan";
      echo json_encode($response);
    }
  } else {
    $response['success'] = 0;
    $response['message'] = "Data Master Gagal disimpan";
    echo json_encode($response);
  }
} else {
  $response['success'] = 0;
  $response['message'] = "Maaf, Terjadi Kesalahan";
  echo json_encode($response);
}
?>