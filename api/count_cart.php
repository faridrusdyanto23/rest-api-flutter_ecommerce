<?php
  $userid = $_GET['userid'];
  include '../config/functions.php';

  // Membuat SQL Query
  $rssql = "SELECT IFNULL(SUM(qty),0) jumlah, IFNULL(SUM(harga),0) totalharga FROM 
  flutter_shopping_cart WHERE userid = '$userid'";

  // Mendapatkan Hasil
  $sql = mysqli_query($con, $rssql);

  // Membuat Array Kosong
  $response = array();
  while($a = mysqli_fetch_array($sql)) {
    $b['jumlah']    =  $a['jumlah'];
    $b['totalharga']    =  $a['totalharga'];
    array_push($response, $b);
  }

  // Menampilkan Array dalam format JSON
  echo json_encode($response);
?>