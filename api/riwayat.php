<?php

//import file
include '../config/functions.php';
header('content-Type: application/json');

$response = array();
$userid = $_GET['userid'];
$sql = mysqli_query($con, "SELECT * FROM flutter_penjualan WHERE userid = '$userid'");

while ($a = mysqli_fetch_array($sql)){
    $id_faktur = $a['id_faktur'];
    $key['id_faktur'] = $id_faktur;
    $key['tgl_jual'] = $a['tgl_jual'];
    $key['grandtotal'] = $a['grandtotal'];
    $key['nilaibayar'] = $a['nilaibayar'];
    $key['nilaikembali'] = $a['nilaikembali'];
    $key['detail'] = array();

    $cek = mysqli_query($con, "SELECT a.*, b.nama_barang, b.image FROM flutter_penjualan_detail a LEFT JOIN flutter_barang b on a.id_barang = b.id_barang WHERE a.id_faktur = '$id_faktur'");
    while ($b = mysqli_fetch_array($cek)){
        $key['detail'][] = array(
            "id" => $b['id'],
            "id_faktur" => $a['id_faktur'],
            "id_barang" => $b['id_barang'],
            "nama_barang" => $b['nama_barang'],
            "image" => $b['image'],
            "qty" => $b['qty'],
            "harga" => $b['harga'],
        );
    }

    array_push($response, $key);
}
echo json_encode($response);

?>