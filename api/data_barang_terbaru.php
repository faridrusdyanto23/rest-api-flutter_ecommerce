<?php
  // import file
  include '../config/functions.php';
  // query sql

  $rssql = "SELECT * FROM flutter_barang b 
            left join flutter_kategori k on b.id_kategori = k.id_kategori 
            left join flutter_satuan s on b.id_satuan = s.id_satuan 
            order by id_barang desc";
  // dapatkan hasil
  $sql = mysqli_query($con, $rssql);
  // deklarasi array
  $response = array();
  $baris = 1;
  while($a = mysqli_fetch_array($sql))
  {
    // memasukan data field kedalam variable
    $b['baris'] = strval($baris);
    $b['id_barang'] = $a['id_barang'];
    $b['id_kategori'] = $a['id_kategori'];
    $b['nama_kategori'] = $a['nama_kategori'];
    $b['id_satuan'] = $a['id_satuan'];
    $b['nama_satuan'] = $a['nama_satuan'];
    $b['nama_barang'] = $a['nama_barang'];
    $b['harga'] = $a['harga'];
    $b['image'] = $a['image'];
    $b['tglexpired'] = $a['tglexpired'];
    array_push($response, $b);
    $baris++;
  }
  echo json_encode($response);

?>
