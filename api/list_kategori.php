<?php
  // import file
  include '../config/functions.php';
  // query sql
  $rssql = "SELECT * FROM flutter_kategori";
  // dapatkan hasil
  $sql = mysqli_query($con, $rssql);
  // deklarasi array
  $response = array();
  $baris = 1;

  while($a = mysqli_fetch_array($sql))
  {
    // memasukan data field kedalam variable
    $b['baris'] = strval($baris);
    $b['id_kategori'] = $a['id_kategori'];
    $b['nama_kategori'] = $a['nama_kategori'];
    array_push($response, $b);
    $baris++;
  }
  echo json_encode($response);

?>
