<?php
  $userid = $_GET['userid'];
  // Import file Koneksi Database
  include '../config/functions.php';

  // Membuat Sql Query
  $rssql = "SELECT DISTINCT(a.id_barang), a.userid, 
    SUM(a.harga) AS harga, SUM(a.qty) AS qty, (SELECT nama_barang FROM 
    flutter_barang WHERE id_barang = a.id_barang) 
    AS nama_barang, (SELECT image FROM flutter_barang WHERE id_barang = a.id_barang) 
    AS gambar FROM flutter_shopping_cart a WHERE a.userid = '$userid' AND id_barang 
    IN (SELECT id_barang FROM flutter_barang) GROUP BY a.id_barang";

  // Mendapatkan Hasil
  $sql = mysqli_query($con, $rssql);

  // Membuat Array Kosong
  $response = array();
  while($a = mysqli_fetch_array($sql)) {
    $b['id_barang'] = $a['id_barang'];
    $b['userid'] = $a['userid'];
    $b['nama_barang'] = $a['nama_barang'];
    $b['gambar'] = $a['gambar'];
    $b['harga'] = $a['harga'];
    $b['qty'] = $a['qty'];
    array_push($response, $b);
  }
  // Menampilkan Array dalam Format JSON
  echo json_encode($response);
?>