<?php
  // import file
  include '../config/functions.php';
  // query sql

  $rssql = "SELECT fb.id_barang,fb.nama_barang,fb.harga,fb.image,SUM(fpd.qty) AS jumlahpenjualan FROM
  flutter_barang fb,flutter_penjualan_detail fpd WHERE fb.id_barang=fpd.id_barang GROUP BY 
  fb.nama_barang ORDER By jumlahpenjualan DESC";

  // dapatkan hasil
  $sql = mysqli_query($con, $rssql);
  // deklarasi array
  $response = array();
  $baris = 1;
  while($a = mysqli_fetch_array($sql))
  {
    // memasukan data field kedalam variable
    $b['baris'] = strval($baris);
    $b['id_barang'] = $a['id_barang'];
    $b['nama_barang'] = $a['nama_barang'];
    $b['harga'] = $a['harga'];
    $b['image'] = $a['image'];
    $b['jumlahpenjualan'] = $a['jumlahpenjualan'];
    array_push($response, $b);
    $baris++;
  }
  echo json_encode($response);

?>
